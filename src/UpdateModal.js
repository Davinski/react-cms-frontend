import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import './UpdateModal.css';

class UpdateModal extends React.Component {
 
  render() {

  	let updateItemText = this.props.isLoading ? 'Loading...' : 'Save';
		let updateItemVariant = this.props.isLoading ? 'outline-warning' : 'outline-success';
		let updateModalClass = this.props.name ? 'update-modal' : 'update-modal slideup';


  	let showModalForm;
  	showModalForm = (
			<div className={updateModalClass}>
				<div className="update-modal-inner">
					<Form className="mt-3">
		      	<h3>Update: {this.props.name}</h3>
		        <Form.Group controlId="formGroupName">
		          <Form.Label>Name</Form.Label>
		          <Form.Control type="text" value={this.props.name}
		           onChange={((e) => this.props.updateName(e, this.props.name))} />
		        </Form.Group>
		        <Form.Group controlId="ControlContent">
		          <Form.Label>Content</Form.Label>
		          <Form.Control as="textarea" rows="3" value={this.props.content}
		          onChange={((e) => this.props.updateContent(e, this.props.content))} />
		        </Form.Group>
		        <Button className="mr-3" variant={updateItemVariant} onClick={this.props.saveUpdate}>{updateItemText}</Button>
				    <Button variant="outline-danger" onClick={this.props.hideModal}>Cancel</Button> 
		      </Form>
	      </div>
      </div>
		);

    return (
      <div>
	  		{
	  			this.props.updateModal === true ? showModalForm : null
	    	}
      </div>
    );
  }
}

export default UpdateModal;
