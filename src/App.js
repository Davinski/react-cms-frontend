import React from 'react';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import UpdateModal from './UpdateModal';
import AddItem from './AddItem';
import './App.css';

class App extends React.Component {
  constructor(props, ...args) {
    super(props, ...args);

    this.state = {
      portfolio: [],
      portfolio_item: {
        id: [],
        name: '',
        content: 'You betcha buddy'
      },
      portfolio_update: {
        id: [],
        name: '',
        content: 'You betcha buddy'
      },
      portfolio_delete: {
        id: []
      },
      isLoading: false,
      saveAlert: false,
      updateModal: false
    };

    this.editName = this.editName.bind(this);
    this.editContent = this.editContent.bind(this);
  }

  saveAlert = _ => {

    this.setState({'saveAlert': true});

    setTimeout(() => {
      this.setState({
        'isLoading': false
      });
    }, 1000);

    setTimeout(() => {
      this.setState({
        'saveAlert': false,
      }); 
    }, 2000);
  }

  componentDidMount() {
    this.getPortfolio();
  }
  
  getPortfolio = _ => {
    fetch('http://localhost:4000/portfolio')
    .then(response => response.json())
    .then(response => this.setState({ portfolio: response.data}))
    .catch(err => console.error(err));
  }

  addPortfolio = _ => {
    const { portfolio_item } = this.state;
    this.setState({'isLoading': true});
    fetch(`http://localhost:4000/portfolio/add?name=${portfolio_item.name}&content=${portfolio_item.content}`)
    .then(this.getPortfolio)
    .then(this.saveAlert())
    .catch(err => console.error(err));
  }

  updatePortfolio = _ => {
    const { portfolio_update } = this.state;
    this.setState({'isLoading': true});
    fetch(`http://localhost:4000/portfolio/update?id=${portfolio_update.id}&name=${portfolio_update.name}&content=${portfolio_update.content}`)
    .then(this.getPortfolio)
    .then(this.saveAlert())
    .catch(err => console.log(err));
  }

  deletePortfolio = _ => {
    const { portfolio_delete } = this.state;
    fetch(`http://localhost:4000/portfolio/delete?id=${portfolio_delete.id}`)
    .then(this.getPortfolio)
    .then(this.setState({
      portfolio_delete: { 
        id: []
      }
    }))
    .catch(err => console.log(err));
  }

  setIdToDelete = (e) => {
    const { portfolio_delete } = this.state;
    let idArray = portfolio_delete.id.slice();

    // checks array for clicked id
    if(idArray.includes(e.target.value)) {
      // loops through id array
      for( var i = 0; i < idArray.length; i++){ 
         // finds the needle in the haystack
         if ( idArray[i] === e.target.value) {
          // removes id if exists
           idArray.splice(i, 1); 
         }
      }
    } else {
      // adds id to array if id not present
      idArray.push(e.target.value);
    }
    // sets id array for deletion
    this.setState({ 
      portfolio_delete: { 
        id: idArray
    }});
  }

  setIdToUpdate = (e) => {

    let selectedKey = e.target.getAttribute('data-key');
    let selectedName = e.target.getAttribute('data-name');
    let selectedContent = e.target.getAttribute('data-content');

    this.setState({
      portfolio_update: {
        id: selectedKey,
        name: selectedName,
        content: selectedContent
      },
      updateModal: true
    });
  }

  editName = ( e, portfolio ) => {

    const { portfolio_item } = this.state;

    this.setState({ 
      portfolio_item: { 
        ...portfolio_item, 
        name: e.target.value,
      }
    });
  }

  editContent = ( e, portfolio ) => {

    const { portfolio_item } = this.state;

    this.setState({ 
      portfolio_item: { 
        ...portfolio_item, 
        content: e.target.value 
      }
    });
  }

  updateName = ( e, portfolio ) => {

    const { portfolio_update } = this.state;

    this.setState({ 
      portfolio_update: { 
        ...portfolio_update, 
        name: e.target.value,
      }
    });
  }

  updateContent = ( e, portfolio ) => {

    const { portfolio_update } = this.state;

    this.setState({ 
      portfolio_update: { 
        ...portfolio_update, 
        content: e.target.value 
      }
    });
  }

  modalClose = () => {

    this.setState({
      portfolio_update: {
        id: '',
        name: '',
        content: '',
      } 
    });

    setTimeout(() => {
      this.setState({updateModal: false})
    }, 300); 

  }


  renderPortfolio = ({ id, name, content }) => {
    return (
      <tr key={id}>
        <td>{id}</td>
        <td>
          <a href="#c" 
            data-key={id} 
            data-name={name} 
            data-content={content} 
            onClick={this.setIdToUpdate}>{name}</a>
        </td>
        <td>{content}</td>
        <td width="50">
          <Form.Check onChange={this.setIdToDelete} name="delete" value={id} />
        </td>
      </tr>
    );
  }


  render() {

    const { portfolio, portfolio_update } = this.state;

    return (
      <div className="App container mt-5">
        <AddItem 
          name={portfolio.name} 
          content={portfolio.content}
          isLoading={this.state.isLoading}
          saveAlert={this.state.saveAlert}
          editName={this.editName} 
          editContent={this.editContent} 
          addPortfolio={this.addPortfolio} 
          deletePortfolio={this.deletePortfolio}
          /> 
        <Table striped bordered hover>
          <thead>
            <tr>
              <th width="30">#</th>
              <th>Name</th>
              <th>Content</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
              {portfolio.map(this.renderPortfolio)} 
          </tbody>
        </Table> 
        <UpdateModal 
          name={portfolio_update.name} 
          content={portfolio_update.content} 
          isLoading={this.state.isLoading}
          saveAlert={this.state.saveAlert}
          hideModal={this.modalClose} 
          updateModal={this.state.updateModal} 
          updateName={this.updateName} 
          updateContent={this.updateContent} 
          saveUpdate={this.updatePortfolio}
          />
      </div>
    );
  }
}

export default App;
