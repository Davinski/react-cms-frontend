import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';

class AddItem extends React.Component {

	render() {

		let addItemText = this.props.isLoading ? 'Loading...' : 'Add Item';
		let addItemVariant = this.props.isLoading ? 'outline-warning' : 'outline-success';

		let saveAlert;
		if (this.props.saveAlert === true) {

			saveAlert = (
				<div className="alert-container">
					<Alert variant="success">Success!</Alert>
				</div>	
			);

		}

		return (
			<div className="mb-4">
				<div className="row">
					<div className="col-md-10"><h3>Add Item</h3></div>
					<div className="col-md-2">
					  {saveAlert}
					</div>
				</div>
	        <div className="row">
	          <div className="col-md-4">
	            <Form.Control 
	              type="text" placeholder="Name" value={this.props.name} 
	              onChange={((e) => this.props.editName(e, this.props.name))} />
	          </div>    
	          <div className="col-md-4">
	            <Form.Control 
	              type="text" placeholder="Content" value={this.props.content} 
	              onChange={((e) => this.props.editContent(e, this.props.content))} />  
	          </div>    
	          <div className="col-md-2">
	            <Button variant={addItemVariant} block onClick={this.props.addPortfolio}>{addItemText}</Button>  
	          </div>    
	          <div className="col-md-2">  
	            <Button onClick={this.props.deletePortfolio} variant="outline-danger" block>Delete</Button> 
	          </div>  
	        </div>
			</div>
		);
	}
}

export default AddItem;